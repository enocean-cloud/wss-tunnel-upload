# EnOcean Serial Tunnel for Cisco AP #

The EnOcean Serial Tunnel Application is an docker based [IOx application](https://developer.cisco.com/docs/app-hosting-ap) for [Cisco Catalyst 9100 Series AP](https://developer.cisco.com/docs/app-hosting-ap/#!device-prerequisites).

![tunnel](img/tunnel.png)

It receives EnOcean sub-gigahertz radio telegrams coming from the EnOcean USB Transceiver and forwards those unprocessed to a given destination. There further data processing is executed. For the current release we support [EnOcean IoT Connector](https://iot.enocean.com) as data egress. The data egress can be in the cloud or in a the local network.

EnOcean Serial Tunnel Application communication works bidirectional. It can also receive packets and send out on the EnOcean radio interface.

The data are encrypted with by the Websockets secure protocol `wss:` (**using TLS/SSL**).

## Operation principle

EnOcean wireless and energy harvesting sensor measure values encode them with an EnOcean Equipment Profile ([EEP]((https://www.enocean-alliance.org/factsheet-eep/) )) and pack them to an [EnOcean radio telegram](https://www.enocean.com/erp1).

The EnOcean USB Transceiver receives the EnOcean radio telegrams. The telegrams are forwarded to Cisco AP on a virtual serial port as byte stream. Then the EnOcean Tunnel Application receives the byte stream, recognizes a [EnOcean serial message](https://www.enocean.com/esp3) and validates it low level integrity - CRC8. No further processing is executed.

![tunnel](img/data-stream.png)

The raw EnOcean serial messages are then forwarded to a secure websocket server, the raw EnOcean payload is encoded in an protobuff scheme. There then the EnOcean serial message is processed and the payload data is extracted from the EnOcean Equipment Profiles [EEP](https://www.enocean-alliance.org/factsheet-eep/) definition.

## Prerequisites
You need to have the correct [Cisco Setup](https://developer.cisco.com/docs/app-hosting-ap/#!device-prerequisites), EnOcean USB Transceivers e,g, [EnOcean USB](https://www.enocean.com/en/products/enocean_modules/) and be able to deploy IOx application on the APs. The APs need to be allowed (firewall) to communicate with URL specified in [configuration](#Configure). The EnOcean Tunnel Application is a secure websocket client and opens a `8080 port` on the target websocket server. The connection then used for the bidirectional communication. 

#### Connect the EnOcean USB Transceiver
The Cisco APs need to enable the inserted EnOcean USB Transceiver for operation.

EnOcean USB Transceivers e.g. `EnOcean USB 300` use the same FTDI driver but different USB PID / VID / VER depending on the device version.
For example, USB 300 DE-12 and the previous versions of the dongle have the following descriptors which are supported by default on Cisco Catalyst 9800 Controller version 17.6.1 or higher:

```
Product ID          : 6001
Vendor ID           : 403
Version             : 600
Manufacturer        : EnOcean GmbH
Description         : USB 300 DE
Driver				: ftdi_sio.ko
```

for USB 300 DE-13 it is the following:

```
Product ID          : 6015
Vendor ID           : 403
Version             : 1000
Manufacturer        : EnOcean GmbH
Description         : USB 300 DE
Driver				: ftdi_sio.ko
```

To confirm if the USB is successfully loaded into your APs you can check by running in you AP shell the `show usb list` command.

```
CiscoAP#show usb list 
PID/VID/VER : Module 
10c4/ea60/100       : cp210x
02/02               : cdc-acm
403/6001/600        : ftdi-sio
User Added
```

If the USB module of your device is not loaded (e.g PID:6015/VID:403/VER:1000 for USB 300 DE-13), you can manually add it through the AP CLI using the command below:

```
CiscoAP#test usb load module ftdi-sio vendor-id 403 product-id 6015 version 1000
CiscoAP#show usb list
PID/VID/VER         : Module
10c4/ea60/100       : cp210x
02/02               : cdc-acm
403/6001/600        : ftdi-sio
User Added
403/6015/1000       : ftdi-sio 
CiscoAP#test usb disable
CiscoAP#test usb enable
```

Note that the vendor-id and product-id in the test command are flipped.

Once you run the command, you need to disable and enable the USB (as shown above) so that the kernel module gets loaded.

If the USB is not listed check with your administrator or Cisco support before you continue.

## Deploy and configure the application
First the application needs to be deployed, afterwards it is configured and then restarted.

### Deploy
The EnOcean Serial Tunnel docker based IOx application is available in the [download](https://bitbucket.org/enocean-cloud/wss-tunnel-upload/downloads/) section.  By downloading the application you agree to the license conditions listed in the `LICENSE` file.

The application is provided as a single `tar` file. The `json` file is only used for activation when using the IOxClient.

A docker based IOx application can be deployed:

- via the Cisco DNA Center management solution. A deployment guide is available [here](https://www.cisco.com/c/en/us/products/collateral/wireless/access-points/guide-c07-744305.html).

- via the IOxClient - for development and debug purposes. Guide is available [here](https://developer.cisco.com/docs/app-hosting-ap/#!deploy-iox-application-on-ap-using-ioxclient)

### Configure

Following paramters are mandtatory:

- `TUNNEL_AUTH_URL` Callback URL for secure websocket authentication. The actual target URL is communication in the authentication process.

- `TUNNEL_USER` Username specified for Websocket authentication.
- `TUNNEL_PASS` Password specified for Websocket authentication.

Following paramters are optional:

- `TUNNEL_CA` If using a self-signed certificate the root certificate `myCA.pem` must be provided for SSL varification

> **_NOTE:_**`TUNNEL_AUTH_URL` target and the data egress e.g. EnOcean IoT Connector can be located in the local network or anywhere in the cloud. No special steps between local or cloud deployment are required. 
>
> When using IoT Connector as data egress the URL needs to have the correct format with path. i.e. `https://<URL or IP>/auth/cisco`. `URL or IP` is the location where the IoTC is deployed.

##### Encoding certificate in base64

```bash
# Go to the directory containing the .pem files
# Use base64 from Ubuntu
# Windows users can use the Ubuntu Docker image to do so
docker run -it --rm -v ${PWD}:/export ubuntu:latest
cd /export

# Linux and Windows users run this command
cat myCA.pem| base64 -w 0 >> myCA.base64.txt
```
Set `TUNNEL_CA` to the contents of `myCA.base64.txt`

It would like this:
```bash
TUNNEL_CA=LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUVMVENDQXhXZ0F3SUJBZ0lKQUl1K0N1Ym5JZFZITUEwR0NTcUdTSWIzRFFFQkN3VUFNSUdzTVFzd0NRWUQKVlFRR0V3SkVSVEVQTUEwR0ExVUVDQXdHUW1GNVpYSnVNUlF3RWdZRFZRUUhEQXRQWW1WeWFHRmphR2x1WnpFawpNQ0lHQTFVRUNnd2JSVzVQWTJWaGJpQkhiV0pJSUVOc2IzVmtJRVJwZG1semFXOXVNUTR3REFZRFZRUUxEQVZECmJHOTFaREVTTUJBR0ExVUVBd3dKYkc5allXeG9iM04wTVN3d0tnWUpLb1pJaHZjTkFRa0JGaDFrWVc1cFpXd3UKZG1sc2JHRnljbVZoYkVCbGJtOWpaV0Z1TG1OdmJUQWVGdzB5TVRBeE1qRXhNekUzTXpSYUZ3MHlOakF4TWpBeApNekUzTXpSYU1JR3NNUXN3Q1FZRFZRUUdFd0pFUlRFUE1BMEdBMVVFQ0F3R1FtRjVaWEp1TVJRd0VnWURWUVFICkRBdFBZbVZ5YUdGamFHbHVaekVrTUNJR0ExVUVDZ3diUlc1UFkyVmhiaUJIYldKSUlFTnNiM1ZrSUVScGRtbHoKYVc5dU1RNHdEQVlEVlFRTERBVkRiRzkxWkRFU01CQUdBMVVFQXd3SmJHOWpZV3hvYjNOME1Td3dLZ1lKS29aSQpodmNOQVFrQkZoMWtZVzVwWld3dWRtbHNiR0Z5Y21WaGJFQmxibTlqWldGdUxtTnZiVENDQVNJd0RRWUpLb1pJCmh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBS1pCRld5bmpXeXoxd284Z050WkIxMDJsR01uVmdaOVhzTFUKRFFjam84YmlXTnhIaUE1dTVEL1hlVTJnNmJVd3JuY0hNME5wVXpJaU5qbTNJNUhoM0VMN3dKNXlLU0E4UTdjRwpPOWo2RXBZTldERnVxMnp5ZmE0TmtxdW9nMjdNbXdJVzBzRkNCNFNuWTdlNnl4YlFXdTlzUWVaRHo5YlFoZE5ICmZXeVdNc1ovM3R4eXV3YktycERQWHllTDFibHo0eHB2V2wweWZrcWFSYzhBejZ1eVMrTlVqM21QZ0ZlUXFFd3YKTjFYcVVxVXkrOWVQcndmL05ZcGdIbXIxbzRZaUtVSy9Rc3dnTlhZaXV1VElYMGNBQUh4UnZ1L1RqakhTZ3dVdgppNnpzRVZUdUh3dlRMMG9ua29KL0MvQ1hHZUFiOFI0djFuZkZscWtocmJscmlIZzdxUzBDQXdFQUFhTlFNRTR3CkhRWURWUjBPQkJZRUZFWGpOSXVJMGRPNEFvQ3NPM1VMNSttbzE5cDNNQjhHQTFVZEl3UVlNQmFBRkVYak5JdUkKMGRPNEFvQ3NPM1VMNSttbzE5cDNNQXdHQTFVZEV3UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQgpBR2ltVWliN0JXNlNxbFdibkUzWnU2ZXg2RWorQlJyMkFoUEZtait5M2pWVnU4dS92cWF4aGJYYko4SHo3eWhSCjYzZ09ENzlwNWxESTFWNXZYWFV1MHBacWdpV0tieTV4V2liUmV0QjFwQWFMTGFYYlZHaVhZOU1Ia2p4czlKelMKOEh3Mm8vdHRQUlF3V3AvcGtQNllob3VuNENNV0w4R3hvbTRpQTBRTG9IZ0JaUTZTSmlRT081RHhINS9DM2hEdgpMcjFnWjBzTzdjWTM2UzFhcFlWVjZOQWJDSjFTSDY4SWF0S0xLcW5ONVBtY2tPb2ZPZzU3bGk5Z2FhODN4WHBICmNQLzN6cDB2dWlxckdISmpwcXBXMUZVYTliZldvRW1CYUV3WjhwUkt5dmtGSERuTlFUUGlzNVA5YXFNZEJPWlAKc0pRQ2l3SndkWTg0N2hxV0RleWtjbE09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
```

#### Prepare the `package_config.ini` file

The configuration is listed inside the `package_config.ini` file. Please prepare the file reflecting you specific parameters.

```shell
#cat package_config.ini

[tunnel]
TUNNEL_AUTH_URL=<your input>
TUNNEL_USER=<your input>
TUNNEL_PASS=<your input>
TUNNEL_CA=<your input>
```


To configure the deployed application you need to upload the `package_config.ini` file to the application & trigger and restart to apply the new settings.

#### Upload the `package_config.ini` file

You execute the upload via the same tool chain used for the deployment:

- via Cisco DNA Center [updated](https://www.cisco.com/c/en/us/td/docs/routers/access/800/software/guides/iox/lm/reference-guide/1-10/b_iox_lm_ref_guide_1_10/b_iox_lm_ref_guide_1_10_chapter_011.html#task_1108277) & [restart](https://www.cisco.com/c/en/us/td/docs/routers/access/800/software/guides/iox/lm/reference-guide/1-10/b_iox_lm_ref_guide_1_10/b_iox_lm_ref_guide_1_10_chapter_011.html#task_1127689) the app to apply the new configurations  
- via IOxClient [reference](https://developer.cisco.com/docs/iox/#!app-management/application-management). e.g. `ioxclient application setconfig tunnel package_config.ini` & restart `ioxclient application restart tunnel `

#### Get Logs files

The EnOcean Tunnel logs application activity and error states into a log file - `wss-tunnel.log`. The log file can be obtained by the same toolset you have used for deployment.

- via Cisco DNA Center [download](https://www.cisco.com/c/en/us/td/docs/routers/access/800/software/guides/iox/lm/reference-guide/1-10/b_iox_lm_ref_guide_1_10/b_iox_lm_ref_guide_1_10_chapter_011.html#task_1108633)
- via IOxClient [download](https://developer.cisco.com/docs/iox/#!app-file-management/managing-application-logs) e.g. `ioxclient application logs download`

Analyzing the logs is also a very good way of debugging a non working installation.

## Additional notes

### License Conditions
The EnOcean Serial Tunnel is provided free of charge for commercial usage. [License conditions](https://bitbucket.org/enocean-cloud/wss-tunnel-upload/downloads/LICENSE) apply. By [downloading](https://bitbucket.org/enocean-cloud/wss-tunnel-upload/downloads/LICENSE) the application you agree to the license conditions listed in the `LICENSE` file.

### Release Notes

#### [Release v1.0.2](https://bitbucket.org/enocean-cloud/wss-tunnel-upload/downloads/)
#### [Release v1.0.0](https://bitbucket.org/enocean-cloud/wss-tunnel-upload/downloads/)

### Alternative Egress points
Any instance supporting the common communication protocols websockets/protobuff can also work with the EnOcean Serial tunnel. If you are interested in using the application with other egress points please [contact us](https://iot.enocean.com/#trial-version).